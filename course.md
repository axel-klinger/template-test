# Satz des Pythagoras

## Darstellung

![Darstellung des Satz des Pythagoras](https://upload.wikimedia.org/wikipedia/commons/d/d1/01-Rechtwinkliges_Dreieck-Pythagoras.svg)  

![Baum am See](https://upload.wikimedia.org/wikipedia/commons/c/c1/Regnitz-Baum-1012073.jpg)

![Petri Netz](https://upload.wikimedia.org/wikipedia/commons/0/08/PetriNetzVentil.png)

## Die Formel

```math
a^2 + b^2 = c^2
```

## PhET example

<iframe src="https://phet.colorado.edu/sims/html/energy-forms-and-changes/latest/energy-forms-and-changes_en.html" width="800" height="600" scrolling="no" allowfullscreen></iframe>

## Beispielvideo (Test)

a) Placeholder with Link to Youtube

[![Entwicklungsumgebung mit Groovy/Git testen](https://img.youtube.com/vi/fbZOii_l7M4/maxresdefault.jpg)](https://youtu.be/fbZOii_l7M4)

b) AV-Portal Player embedded

<iframe width="560" height="315" scrolling="no" src="//av.tib.eu/player/40456" frameborder="0" allowfullscreen="allowfullscreen"></iframe>

c) Youtube embedded

<iframe width="420" height="315"
src="https://www.youtube.com/embed/fbZOii_l7M4" allowfullscreen="allowfullscreen">
</iframe>

## H5P Test

<iframe src="https://h5p.org/h5p/embed/617" width="545" height="337" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

...

<iframe src="https://h5p.org/h5p/embed/62954" width="1090" height="480" frameborder="0" allowfullscreen="allowfullscreen"></iframe><script src="https://h5p.org/sites/all/modules/h5p/library/js/h5p-resizer.js" charset="UTF-8"></script>

## Anwendung

* Beipsiel

## Aufgabe

* Beispielaufgabe mit Lösung
